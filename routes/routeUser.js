"use strict";

const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");


// route GET /users

router.get("/users", userController.getUsers);

// route GET /users/ profil

router.get("/users/profil", userController.getProfil);

// route GET /users/:id

router.get("/users/:id", userController.getUser);

// route PUT /users

router.put("/users/:id", userController.putUser);

// route DELETE /users

router.delete("/users/:id", userController.deleteUser);

module.exports = router;
