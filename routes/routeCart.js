"use strict";

const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
// isAuth est un middleware qui vérifie si l'utilisateur est authentifié

// GET / cart

router.get("/cart", cartController.getCart);

// PUT / cart

router.put("/cart",  cartController.putCart);

// DELETE / cart/:id

router.delete("/cart/:id",  cartController.deleteCart);

module.exports = router;
