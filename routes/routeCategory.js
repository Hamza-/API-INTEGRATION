"use strict";

const express = require("express");
const router = express.Router();
const categoriesController = require("../controllers/categoryController");

// route /categories

router.get("/categories", categoriesController.getCategory);

// route /categories/:id

router.get("/categories/:id", categoriesController.getCategoryId);

// route /categories/add

router.post("/categories",   categoriesController.addCategory);

// route /categories/:id/edit

router.put("/categories/:id",   categoriesController.putCategory);

// route /categories/:id/delete

router.delete("/categories/:id",   categoriesController.deleteCategoryById);

module.exports = router;
