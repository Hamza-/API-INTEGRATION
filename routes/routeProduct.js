"use strict";

const express = require("express");
const router = express.Router();
const productsController = require("../controllers/productController"); 


// GET /produits

router.get("/products", productsController.getProducts);

// GET /products/user/:userId

router.get("/products/user/:userId", productsController.getProductsByUserId);

// GET /produits/:id

router.get("/products/:id", productsController.getProduct);

// POST /produits

router.post("/products", productsController.addProduct);

// PUT /produits/:id

router.put("/products/:id", productsController.updateProduct);

// DELETE /produits/:id

router.delete("/products/:id", productsController.deleteProduct);

module.exports = router;
